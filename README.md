# Contao Event Manager Bundle

## About
Mit diesem Bundle können Frontend User Veranstaltungen (Events) im Frontend erstellen, bearbeiten und löschen.

### Features
- [x] Im Frontend Events erstellen, bearbeiten und löschen
- [x] Event Kategorien
- [x] Zuordnung von Kategorien zu Frontend User 
- [x] Event Erfassen Formular mit Haste (eigenes Template) 
- [x] REST Api with JSON Response 

## REST API
---
Es gibt eine einfache REST API. folgende Abfragen sind möglich:

| Route      | Description
| :---        |    :--- 
| memoEventManager/REST/events/{ArchiveID}      | Liste aller Events aus Archiv mit ID     
| memoEventManager/REST/events/{ArchiveID}/{CategorieID}      | Liste aller Events aus Archiv mit ID und Kategorie     
| memoEventManager/REST/event/{EventID}   | Event Detail      



## Installation
Install [composer](https://getcomposer.org) if you haven't already.
Add the unlisted Repo (not on packagist.org) to your composer.json:
```
"repositories": [
  {
    "type": "git",
    "url" : "https://bitbucket.org/memo_development/contao-eventmanager-bundle"
  }
],
```

Add the bundle to your requirements:
```
"memo_development/contao-eventmanager-bundle": "dev-master",
```

## Usage (German)
1. Memo Event Manager Bundle installieren



## Contribution
Bug reports and pull requests are welcome

