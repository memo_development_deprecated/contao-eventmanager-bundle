<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) Media Motion AG
 *
 * @package   EventManagerBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\EventManagerBundle\Module;

use Contao\FrontendUser;
use Memo\CategoryBundle\Model\CategoryModel;


class ModuleEventKategorieFilter extends \Module
{

    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'mod_event_categorie_filter';

    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new \Contao\BackendTemplate('be_wildcard');
            $objTemplate->wildcard = $strWildcard = 'An dieser Stelle erscheinen im Frontend die <a href="/contao?do=calendar&table=tl_calendar_events&id=1"><strong>hier verwalteten Inhalte</strong></a>.';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;

            return $objTemplate->parse();
        }
        return parent::generate();
    }

    protected function compile()
    {
        if(empty($this->eventCategories)) {
            $aCategories = [];
        }else {
            $aCategories = unserialize($this->eventCategories);
        }

        $aCat = [];
        $oCategories = CategoryModel::findAll(['order'=>'title ASC']);

        foreach($oCategories as $key => $val)
        {
            if(in_array($val->id,$aCategories) OR in_array($val->pid,$aCategories)) {
                $aCat[] = $val;
            }
        }

        $this->Template->headline = $this->headline;
        $this->Template->categories = $aCat;
    }
}
