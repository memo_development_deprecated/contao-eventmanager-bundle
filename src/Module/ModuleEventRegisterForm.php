<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) Media Motion AG
 *
 * @package   EventManagerBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\EventManagerBundle\Module;

use Contao\FrontendUser;
use Memo\CategoryBundle\Model\CategoryModel;


class ModuleEventRegisterForm extends \Module
{

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'mod_event_register_form';

	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new \Contao\BackendTemplate('be_wildcard');
			$objTemplate->wildcard = $strWildcard = 'An dieser Stelle erscheinen im Frontend die <a href="/contao?do=calendar&table=tl_calendar_events&id=1"><strong>hier verwalteten Inhalte</strong></a>.';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;

			return $objTemplate->parse();
		}
		return parent::generate();
	}

	protected function compile()
	{

		$this->Template->form = null;
		$this->Template->blnLoggedIn = true;
        $objUser        = FrontendUser::getInstance();
        $aAllowedGroups = unserialize($this->groups);

		if (TL_MODE === 'FE')
		{
            $blnAccessAllowed = true;
            if(!empty($aAllowedGroups) && $this->protected){
                $blnAccessAllowed = false;
                foreach($objUser->groups as $key => $val)
                {
                   if(in_array($val,$aAllowedGroups)) {
                       $blnAccessAllowed = true;
                       break;
                   }
                }
            }
			if (FE_USER_LOGGED_IN === true && $blnAccessAllowed === true) {

				$objForm = new \Haste\Form\Form('createEventItem', 'POST', function ($objHaste) {
					return \Input::post('FORM_SUBMIT') === $objHaste->getFormId();
				});

                //Get Values | edit Item
                $oEvent  = null;
                $selItem = null;

                $prmItem = \Input::get('item');
                if(!is_null($prmItem))
                {
                    //get Values
                    $oEvent = \Contao\CalendarEventsModel::findByPk(intval($prmItem));
                    $selItem = unserialize($oEvent->categories)[0];
                }

                //add categorie id
                $aEventCat = CategoryModel::findBy('pid',6);
                foreach($aEventCat as $key => $val)
                {
                    $aEventCatOptions[$val->id] = $val->title;
                }
                $objForm->addFormField('categories', array(
                    'name' => 'categorie',
                    'label' => 'Kategorie',
                    'inputType' => 'checkbox',
                    'ignoreModelValue' => true,
                    'value' => $selItem,
                    'options' => $aEventCatOptions
                ));


				//generate form from DCA
				$objForm->addFieldsFromDca('tl_calendar_events', function (&$strField, &$arrDca) {
					//allowed fields 4 form
					$aFields = ['title', 'teaser', 'startDate','endDate', 'address','location'];

					//Get Values | edit Item
					$oEvent = null;
					$selItem = null;

					$prmItem = \Input::get('item');
					if(!is_null($prmItem))
					{
						//get Values
						$oEvent = \Contao\CalendarEventsModel::findByPk(intval($prmItem));
                        $selItem = unserialize($oEvent->categories)[0];
					}

					// make sure to skip elements without inputType or you will get an exception
					if (in_array($strField, $aFields)) {
						if(!is_null($oEvent))
						{
							if($strField == 'title')
							{
								$arrDca['value'] = $oEvent->title;
							}
							if($strField == 'teaser')
							{
								$arrDca['value'] = $oEvent->teaser;
							}
							if($strField == 'startDate')
							{
								$arrDca['value'] = $oEvent->startDate;
							}
                            if($strField == 'endDate')
                            {
                                $arrDca['value'] = $oEvent->endDate;
                            }
							if($strField == 'address')
							{
								$arrDca['value'] = $oEvent->address;
							}
							if($strField == 'location')
							{
								$arrDca['value'] = $oEvent->location;
							}
						}
						return true;
					}
					// skip other fields
					return false;
				});


                $uploadFolder = \Dbafs::addResource("files/member-uploads");
                $objForm->addFormField('upload_file', array(
                    'label'         => 'Datei-Upload',
                    'inputType'     => 'upload',
                    'eval'          => array('extensions'=>'jpg,jpeg,gif,png', 'storeFile'=>true, 'uploadFolder'=> $uploadFolder->uuid, 'doNotOverwrite' => true, 'maxlength' => 2048000)
                ));

                //Add record ID 4 edit
				if($prmItem = \Input::get('item'))
				{
					$objForm->addFormField('itemID', array(
			            'name' => 'id',
			            'inputType' => 'hidden',
			            'ignoreModelValue' => true,
			            'value' => $prmItem
			        ));
				}


				$objForm->addSubmitFormField('submit', 'Veranstaltung eintragen');
				$objForm->addCaptchaFormField('captcha');
                $blnNew = false;
                $arrTokens = [];
				if ($objForm->validate()) {

					// Get all the submitted and parsed data (only works with POST):
					$arrData = $objForm->fetchAll();

					$prmItemID = \Input::post('id');
					if(empty($prmItemID))
					{
						//Create New Event
						$objCe = new \CalendarEventsModel();
						$objCe->status = 1;
						$objCe->author = 2; //Media Motion AG
						$objCe->owner	 	= $objUser->id;
						$objCe->pid		 	= 1;
						$objCe->published 	= 1;
						$objCe->tstamp 	    = time();
						$objCe->startTime 	= strtotime($arrData['startDate']);
						$objCe->endTime 	= strtotime($arrData['startDate'])+86399;
                       $blnNew = true;

					}else{
						$objCe = \Contao\CalendarEventsModel::findByPk(intval($prmItemID));
					}

					$objCe->startDate 	= strtotime($arrData['startDate']);
                    $arrTokens['form_endDate'] = "";
					if(!empty($arrData['endDate'])) {
                        $objCe->endDate = strtotime($arrData['endDate']);
                        $objCe->endTime = strtotime($arrData['endDate'])+86399;
                        $arrTokens['form_endDate'] = date("d.m.Y",$objCe->endDate);
                    }

					//FileUpload

                    if ($_SESSION['FILES']['upload_file']['name']) {
                        $objCe->singleSRC = \Dbafs::addResource($uploadFolder->path . "/" . $_SESSION['FILES']['upload_file']['name'])->uuid;
                        $objCe->addImage 	= true;
                        $arrTokens['form_image'] = $uploadFolder->path . "/" . $_SESSION['FILES']['upload_file']['name'];
                    }

                    if(is_array(\Input::post('categorie'))) {
                        $objCe->categories = serialize(\Input::post('categorie'));
                    }else{
                        $objCe->categories = serialize([\Input::post('categorie')]);
                    }

					$objCe->title 		= trim($arrData['title']);
                    $objCe->teaser      = trim(\StringUtil::decodeEntities($arrData['teaser']));
					$objCe->address	 	= trim($arrData['address']);
					$objCe->location 	= trim($arrData['location']);
					$objCe->save();


                    //Send Notification
                    if(!empty($this->notification) and is_numeric($this->notification)) {
                        $oNotification = \NotificationCenter\Model\Notification::findByPk($this->notification);
                        $arrTokens['form_title']    = $objCe->title;
                        $arrTokens['form_teaser']   = $objCe->teaser;
                        $arrTokens['form_teaser_html']   = \StringUtil::decodeEntities($objCe->teaser);
                        $arrTokens['form_address']  = $objCe->address;
                        $arrTokens['form_location'] = $objCe->location;
                        $arrTokens['form_startDate'] = date("d.m.Y",$objCe->startDate);

                        $arrTokens['form_categories'] = implode(", ", unserialize($objCe->categories));

                        if (null !== $oNotification) {
                            $oNotification->send($arrTokens);
                        }
                    }

                    //Redirect to OK-Page
                    if($this->jumpTo) {
                        $urlGenerator = \System::getContainer()->get('contao.routing.url_generator');
                        $oOkPage = \PageModel::findById($this->jumpTo);
                        \Controller::redirect($urlGenerator->generate($oOkPage->alias));
                    }
				}


				// Get the form as string
				$this->Template->form = $objForm->generate();
			}else{
				$this->Template->blnLoggedIn = false;
				\System::log("Create FE Event view not allowed",__METHOD__,TL_ACCESS);
			}
			$this->Template->headline = $this->headline;

		}
	}
}
