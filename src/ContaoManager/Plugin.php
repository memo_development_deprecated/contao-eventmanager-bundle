<?php

declare(strict_types=1);

/**
 * @package   EventManagerBundle
 * @author    Media Motion AG, Christian Nussbaumer
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\EventManagerBundle\ContaoManager;

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Contao\ManagerPlugin\Routing\RoutingPluginInterface;
use Symfony\Component\Config\Loader\LoaderResolverInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\RouteCollection;
use Contao\CalendarBundle\ContaoCalendarBundle;
use Memo\EventManagerBundle\EventManagerBundle;

/**
 * @internal
 */
class Plugin implements BundlePluginInterface, RoutingPluginInterface
{
    public function getBundles(ParserInterface $parser): array
    {
        return [
            BundleConfig::create(EventManagerBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class,ContaoCalendarBundle::class]),
        ];
    }

    public function getRouteCollection(LoaderResolverInterface $resolver, KernelInterface $kernel): RouteCollection
    {
        return $resolver
            ->resolve(__DIR__.'/../Resources/config/routes.yml')
            ->load(__DIR__.'/../Resources/config/routes.yml')
            ;
    }
}
