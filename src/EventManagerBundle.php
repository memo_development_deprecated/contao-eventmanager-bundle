<?php
/**
 * @package   EventManagerBundle
 * @author    Media Motion AG, Christian Nussbaumer
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\EventManagerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class EventManagerBundle extends Bundle
{
}
