<?php
/**
 * @package   EventManagerBundle
 * @author    Media Motion AG, Christian Nussbaumer
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\EventManagerBundle\EventListener;

use Contao\Controller;
use Contao\Template;

class ParseTemplateListener
{
	/**
	 *
	 */
	public function addStructureClasses(Template $template): void
	{
		$strAdditionalClasses = '';

		if ($template->structureWidth && $template->inColumn === 'main' || 'ce_text' == $template->getName() && $template->inColumn === 'main' && $template->ptable != 'tl_node') {
			$strAdditionalClasses = '';

			if ($template->structureWidth) {
				switch ($template->structureWidth) {
					case 'inset':
						$strAdditionalClasses = 'push-right-2 push-left-2 push-right-medium-0 push-left-medium-0';
						break;
					case 'outset':
						$strAdditionalClasses = '';
						break;
					case 'base-width':
						$strAdditionalClasses = 'base-width';
						break;
				}
			}

			$template->class .= ' ' . $strAdditionalClasses;
		}

		if ('ce_text' === $template->getName() || 'ce_headline' === $template->getName()) {
			if ($template->textAlign) {
				$template->class .= ' ' . $template->textAlign;
			}
		}


	}

	/**
	 *
	 */
	public function addStylingClasses(Template $template): void
	{
		if ($template->hyperlinkStyle && strpos($template->getName(), 'ce_hyperlink') !== false) {
			switch ($template->hyperlinkStyle) {
				case 'block':
					$template->setName('ce_hyperlink_block');
					break;
				case 'inline':
					$template->setName('ce_hyperlink_inline');
					break;
			}
		} elseif ($template->hyperlinkStyle) {

		}

		if ($template->titleFormat && strpos($template->getName(), 'ce_headline') !== false) {
			$template->class .= ' ' . $template->titleFormat;
		}
	}

	/**
	 * Callback for custom nav rsce
	 * @param Template $template
	 */
	public function setCustomNavLinks(Template $template)
	{
		if ($template->customPagesSortOrder && strpos($template->getName(), 'rsce_custom_navigation') !== false) {
			$arrPages = unserialize($template->customPagesSortOrder);
			if (!empty($arrPages)) {
				$colPages = \Contao\PageModel::findMultipleByIds($arrPages);

				$template->pages = $colPages;

				foreach ($colPages as $key => $objPage) {
					$arrImages = unserialize($objPage->pageImage);

					if (!empty($arrImages)) {
						$objFile = \Contao\FilesModel::findByUuid($arrImages[0]);

						$objPage->pageImage = $objFile->id;
					}
				}


				if ($colPages) {
					$arrPages = $colPages;
				}
			}

		}
	}
}
