<?php


namespace Memo\EventManagerBundle\Controller;

use Contao\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Memo\CategoryBundle\Model\CategoryModel;

class MemoEventManagerRestController extends AbstractController
{

    /**
     * Liste aller aktiven Events der ausgewählten Kategorie
     * @param integer $calId
     * @param null $catId
     * @return JsonResponse
     */
    public function getEventListByCategorie($calId = NULL, $catId = NULL): JsonResponse
    {

        //Kein Kalender gewählt
        if (empty($calId)) {
            return new JsonResponse();
        }
        //Default alle Events
        if (empty($catId)) {
            return $this->getEventList();
        }

        $aReturn = [];

        //Initialize Contao
        $this->get('contao.framework')->initialize();
        $oData = \CalendarEventsModel::findUpcomingByPids([$calId]);
        $aCategories = $this->_getEventCategories();

        if ($oData) {
            foreach ($oData as $key => $val) {
                $aItemCat = unserialize($val->categories);
                if (in_array($catId, $aItemCat)) {
                    //Add Categorie Infos
                    $aTmpCat = [];
                    for ($i = 0; $i < count($aItemCat); $i++) {
                        if (isset($aCategories[$aItemCat[$i]])) {
                            $aTmpCat[] = $aCategories[$aItemCat[$i]];
                        }
                    }
                    $val->categories = $aTmpCat;
                    $val->files = $this->getFilepaths($val);

                    $aRow = $this->modifyItemProperties($val->row());

                    $aReturn[] = $aRow;
                }
            }
        }

        $response = new JsonResponse();
        $response->setContent(json_encode($aReturn, JSON_INVALID_UTF8_SUBSTITUTE));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Liste aller aktiven Events
     * @param integer $calId
     * @return JsonResponse
     */
    public function getEventList($calId = NULL): JsonResponse
    {
        $aReturn = [];
        $aCategories = [];
        $prmCalendarId = 1;
        if (empty($prmCalendarId)) {
            return new JsonResponse($aReturn);
        }
        //Initialize Contao
        $this->get('contao.framework')->initialize();
        $oData = \CalendarEventsModel::findUpcomingByPids([1]);
        $aCategories = $this->_getEventCategories();

        foreach ($oData as $key => $val) {
            $aItemCat = unserialize($val->categories);
            //Add Categorie Infos
            $aTmpCat = [];
            for ($i = 0; $i < count($aItemCat); $i++) {
                if (isset($aCategories[$aItemCat[$i]])) {
                    $aTmpCat[] = $aCategories[$aItemCat[$i]];
                }
            }
            $val->categories = $aTmpCat;
            $val->files = $this->getFilepaths($val);

            $aRow = $this->modifyItemProperties($val->row());

            $aReturn[] = $aRow;
        }

        $response = new JsonResponse();
        $response->setContent(json_encode($aReturn, JSON_INVALID_UTF8_SUBSTITUTE));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Modify content. Eg. replace inserttags.
     * @param $arrItem
     * @return mixed
     */
    private function modifyItemProperties($arrItem)
    {
        if ($arrItem['teaser']) {
            $arrItem['teaser'] = \Template::replaceInsertTags($arrItem['teaser']);
        }

        return $arrItem;
    }

    private function getFilepaths($event)
    {
        $arrFiles = [];

        $arrFileKeys = [
            'singleSRC',
        ];

        foreach ($arrFileKeys as $strFileKey) {
            if ($event->{$strFileKey} && $objFile = \FilesModel::findByPk($event->{$strFileKey})) {
                $arrFiles[$strFileKey] = $objFile->row();
            }
        }


        if ($arrAttachments = unserialize($event->orderEnclosure)) {
            $arrFiles['attachments'] = [];

            foreach ($arrAttachments as $strFileId) {
                if ($objFile = \FilesModel::findByPk($strFileId)) {
                    $item = $objFile->row();

                    $item['meta'] = unserialize($item['meta']);

                    $arrFiles['attachments'][] = $item;
                }
            }
        }

        return $arrFiles;
    }

    /**
     * Get Event Detail
     * @param null $eventId
     * @return JsonResponse
     */
    public function getEventDetailById($eventId = NULL)
    {
        if (empty($eventId)) {
            return new JsonResponse();
        }
        $aReturn = [];

        //Initialize Contao
        $this->get('contao.framework')->initialize();
        $oData = \CalendarEventsModel::findByPk($eventId);
        $oData->files = $this->getFilepaths($oData);
        $aReturn = $oData->row();

        //Add Categorie Infos
        $aCategories = $this->_getEventCategories();

        $aTmpCat = [];
        $aItemCat = unserialize($aReturn['categories']);
        $aReturn['categories'] = [];
        for ($i = 0; $i < count($aItemCat); $i++) {
            if (isset($aCategories[$aItemCat[$i]])) {
                $aReturn['categories'][] = $aCategories[$aItemCat[$i]];
            }
        }

        $aReturn = $this->modifyItemProperties($aReturn);

        $response = new JsonResponse();
        $response->setContent(json_encode($aReturn, JSON_INVALID_UTF8_SUBSTITUTE));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function getImageByIdAndSize($imageId, $size)
    {
        if (empty($imageId) || empty($size)) {
            return new JsonResponse();
        }

        $aReturn = [];

        //Initialize Contao
        $this->get('contao.framework')->initialize();
        $oData = \FilesModel::findByPk($imageId);

        $template = new \FrontendTemplate();

        Controller::addImageToTemplate($template, [
            'singleSRC' => $oData->path,
            'size' => ['', '', $size],
        ], null, null, $oData);

        $response = new JsonResponse();
        $response->setContent(json_encode($template->getData(), JSON_INVALID_UTF8_SUBSTITUTE));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function getRssEventListByCategorie($catId = NULL)
    {

    }

    /**
     * Get All Categories as array with key=categorie ID
     * @return array
     */
    private function _getEventCategories()
    {
        $oCategories = NULL;
        $aCategories = [];
        $oCategories = CategoryModel::findAll();

        if ($oCategories) {
            //format as array with key = categorie ID
            foreach ($oCategories as $key => $val) {
                $aCategories[$val->id] = $val->row();
            }
        }
        return $aCategories;
    }

}
