<?php

namespace Memo\EventManagerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;


class MemoEventManagerController extends AbstractController
{

    /**
     * Löscht einen Kalendereintrag wenn der eingeloggte User die Berechtigung dazu hat.
     * Redirect auf Referer Seite
     * @param Request $req
     * @return RedirectResponse
     */
   public function deleteCalendarEvent(Request $req): RedirectResponse
   {
       //Initialize Contao
       $this->get('contao.framework')->initialize();
       $objUser = \FrontendUser::getInstance();
       $refererPageId = $req->query->get('page');
       $eventId       = $req->query->get('item');

       if(!empty($eventId) and $objUser->id) {
           $oEvent = \CalendarEventsModel::findByPk($eventId);
           $aEventCat = unserialize($oEvent->categories);
           //Check Access for user
           if($oEvent->owner === $objUser->id)
           {
               //Delete Event
               $oEvent->delete();
           }
       }

       return new RedirectResponse($req->server->get('HTTP_REFERER'));
   }

}
