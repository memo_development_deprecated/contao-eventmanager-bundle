<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package   EventManagerBundle
 * @author    Media Motion AG, Christian Nussbaumer
 * @license   MEMO
 * @copyright Media Motion AG
 */

/**
 * Add back end modules
 */


/**
 * Add front end modules
 */

$GLOBALS['FE_MOD']['memo_event'] = array
(
	'memo_event_register_form'    => 'Memo\EventManagerBundle\Module\ModuleEventRegisterForm',
	'memo_event_categorie_filter'    => 'Memo\EventManagerBundle\Module\ModuleEventKategorieFilter'
);

/**
 * Models
 */

/**
 * HOOKS
 */
