<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) Media Motion AG
 *
 * @package   EventManagerBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

$dca = &$GLOBALS['TL_DCA']['tl_calendar_events'];
$dca['palettes']['default'] = str_replace('author', 'author,owner', $dca['palettes']['default']);
$dca['palettes']['default'] = str_replace('endDate', 'endDate;{categories_legend},categories;', $dca['palettes']['default']);
$dca['palettes']['internal'] = str_replace('endDate', 'endDate;{categories_legend},categories;', $dca['palettes']['internal']);
$dca['palettes']['article'] = str_replace('endDate', 'endDate;{categories_legend},categories;', $dca['palettes']['article']);
$dca['palettes']['external'] = str_replace('endDate', 'endDate;{categories_legend},categories;', $dca['palettes']['external']);


$dca['fields']['categories'] = [
    'exclude' 					=> true,
    'search'					=> false,
    'inputType'					=> 'checkbox',
    'foreignKey'				=> 'tl_memo_category.title',
    'options_callback'			=> ['tl_calendar_events_ext', 'getEventCategories'],
    'eval'						=> array(
        'multiple' => true,
        'chosen' => true,
        'includeBlankOption' => true,
        'tl_class' => 'clr long'
    ),
    'sql'						=> "blob NULL"
];

$dca['fields']['owner'] = [
    'exclude' 					=> true,
    'filter'					=> true,
    'inputType'					=> 'select',
    'foreignKey'				=> "tl_member.CONCAT(firstname,' ',lastname)",
    'eval'						=> array(
        'multiple' => false,
        'chosen' => true,
        'includeBlankOption' => true,
        'tl_class' => 'clr long'
    ),
    'sql'						=> "blob NULL"
];

use Memo\CategoryBundle\Service\CategoryService;

class tl_calendar_events_ext extends Backend {
    public function getEventCategories($dc){

        $oEvent = CalendarEventsModel::findByPk($dc->id);
        $oEventArchive = CalendarModel::findByPk($oEvent->pid);
        $aSelectedCategories = unserialize($oEventArchive->categories);

        return CategoryService::getAllCategories(true,$aSelectedCategories);
    }
}


?>
