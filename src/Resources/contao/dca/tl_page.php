<?php

/**
 * Hotfix for missing Google Map API
 */
//todo: Remove this Hotfix as soon as it is fixed in delahaye/dlh_googlemaps
$GLOBALS['TL_DCA']['tl_page']['palettes']['rootfallback'] = str_replace('{global_legend', '{dlh_googlemaps_legend},dlh_googlemaps_apikey,dlh_googlemaps_protected;{global_legend', $GLOBALS['TL_DCA']['tl_page']['palettes']['rootfallback']);
