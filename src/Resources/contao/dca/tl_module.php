<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) Media Motion AG
 *
 * @package   EventManagerBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

$GLOBALS['TL_DCA']['tl_module']['palettes']['memo_event_register_form'] = '{title_legend}, name,type,headline;{redirect_legend},jumpTo;{notification_legend:hide},notification;{template_legend:hide},customTpl;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID';
$GLOBALS['TL_DCA']['tl_module']['palettes']['memo_event_categorie_filter'] = '{title_legend}, name,type,headline;{categories_legend},eventCategories;{template_legend:hide},customTpl;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID';

$GLOBALS['TL_DCA']['tl_module']['fields']['eventCategories'] = [
    'exclude' 					=> true,
    'filter'					=> false,
    'inputType'					=> 'checkbox',
    'foreignKey'				=> 'tl_memo_category.title',
    'options_callback'			=> ['memo.foundation.category', 'getAllCategories'],
    'eval'						=> array(
        'multiple' => true,
        'chosen' => true,
        'includeBlankOption' => true,
        'tl_class' => 'clr long'
    ),
    'sql'						=> "blob NULL"
];

$GLOBALS['TL_DCA']['tl_module']['fields']['notification'] = [
    'exclude'					=> true,
    'inputType'					=> 'select',
    'eval'						=> array('mandatory'=>false, 'tl_class'=>'w50 clr','includeBlankOption'=>true, 'multiple'=>false, 'chosen'=>true),
    'foreignKey'			    => 'tl_nc_notification.title',
    'sql'						=> "varchar(255) NOT NULL default ''"
];
