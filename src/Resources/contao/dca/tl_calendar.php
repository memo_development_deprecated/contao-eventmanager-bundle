<?php
/**
 * Contao Open Source CMS
 *
 * Copyright (c) Media Motion AG
 *
 * @package   EventManagerBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

$dca = &$GLOBALS['TL_DCA']['tl_calendar'];
$dca['palettes']['default'] = str_replace('jumpTo;', 'jumpTo;{categories_legend},categories;', $dca['palettes']['default']);


$dca['fields']['categories'] = [
    'exclude' 					=> true,
    'search'					=> false,
    'inputType'					=> 'checkbox',
    'foreignKey'				=> 'tl_memo_category.title',
    'options_callback'			=> ['tl_calendar_ext', 'getEventCategories'],
    'eval'						=> array(
        'multiple' => true,
        'chosen' => true,
        'includeBlankOption' => true,
        'tl_class' => 'clr long'
    ),
    'sql'						=> "blob NULL"
];


use Memo\CategoryBundle\Service\CategoryService;

class tl_calendar_ext extends Backend {

    public function getEventCategories(){
        //only Category Parent ID 1
        return CategoryService::getCategoryGroups();
    }
}
