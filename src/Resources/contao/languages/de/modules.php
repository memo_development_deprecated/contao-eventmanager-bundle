<?php
/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @package   EventManagerBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Back end modules
 */

/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['memo_event'] = ['MEMO Event-Manager',''];
$GLOBALS['TL_LANG']['FMD']['memo_event_register_form'] = ['Event-Formular (Eingabe)',''];
$GLOBALS['TL_LANG']['FMD']['memo_event_categorie_filter'] = ['Event-Kategorie Filter',''];

